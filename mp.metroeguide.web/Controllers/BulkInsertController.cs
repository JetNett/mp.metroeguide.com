﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mp.metroeguide.web.Controllers
{
    public class BulkInsertController : Controller
    {
        // GET: BulkInsert
        public ActionResult Index()
        {
            ViewBag.ActivePage = "bulk";
            return View();
        }
    }
}
