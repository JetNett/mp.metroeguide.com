﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mp.metroeguide.web.Controllers
{
    public class LinkViewerController : Controller
    {
        // GET: LinkViewer
        public ActionResult Index(int? id)
        {
            if(id != null)
            {
                ViewBag.PageId = id;
            }


            ViewBag.ActivePage = "linkViewer";

            return View();
        }
    }
}