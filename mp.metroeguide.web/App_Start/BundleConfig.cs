﻿using System.Web;
using System.Web.Optimization;

namespace mp.metroeguide.web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            //CSS
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/toastr.min.css"));


            //JS
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/vendor/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/vendor/bootstrap.js",
                      "~/Scripts/vendor/respond.js"));


            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                "~/Scripts/vendor/knockout-{version}.js",
                "~/Scripts/koHandlers/showModal.js",
                "~/Scripts/vendor/pager.min.js",
                "~/Scripts/vendor/moment.js",
                "~/Scripts/vendor/toastr.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                "~/Scripts/dataService.js",
                "~/Scripts/reusables/foldersList.js"));

            bundles.Add(new ScriptBundle("~/bundles/clientManager").Include(
                "~/Scripts/clientManager/models.js",
                "~/Scripts/clientManager/clientVm.js"));

            bundles.Add(new ScriptBundle("~/bundles/pageCopier").Include(
                "~/Scripts/copy/models.js",
                "~/Scripts/copy/copyVm.js"));

            bundles.Add(new ScriptBundle("~/bundles/manage").Include(
                "~/Scripts/reusables/foldersList.js",
                "~/Scripts/manage/models.js",
                "~/Scripts/manage/manageVm.js"));

            bundles.Add(new ScriptBundle("~/bundles/pageManage").Include(
                "~/Scripts/pageManage/models.js",
                "~/Scripts/pageManage/pageManageVm.js"));

            bundles.Add(new ScriptBundle("~/bundles/linkViewer").Include(
                "~/Scripts/koHandlers/iFrameBinder.js",
                "~/Scripts/models/LinkModel.js",
                "~/Scripts/linkViewer/linkViewerVm.js"));

            bundles.Add(new ScriptBundle("~/bundles/findReplace").Include(
                "~/Scripts/findReplace/findReplaceVm.js"));

            bundles.Add(new ScriptBundle("~/bundles/linkChecker").Include(
                "~/Scripts/koHandlers/img.js",
                "~/Scripts/linkChecker/models.js",
                "~/Scripts/linkChecker/linkCheckerVm.js"));

        }
    }
}
